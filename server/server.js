'use strict'

process.title = 'node-chat'

const webSocketsServerPort = 9000

// websocket and http servers
const webSocketServer = require('websocket').server
const http = require('http')

let history_messages = []
const clients = []

/**
 * HTTP server
 */
const server = http.createServer(function(request, response) {
  if (request.url === '/status') {
    response.writeHead(200, { 'Content-Type': 'application/json' })
    let responseObject = {
      currentClients: clients.length,
      totalHistory: history_messages.length
    }
    response.end(JSON.stringify(responseObject))
  }
})
server.listen(webSocketsServerPort, () => {
  console.log(new Date() + ' Server is listening on port ' + webSocketsServerPort)
})

/**
 * WebSocket server
 */
const wsServer = new webSocketServer({
  httpServer: server
})

wsServer.on('request', request => {
  console.log(new Date() + ' Connection from origin ' + request.origin + '.')

  const connection = request.accept(null, request.origin)
  connection.index = clients.push(connection) - 1

  if (history_messages.length > 0) {
    connection.sendUTF(JSON.stringify({ type: 'history', data: history_messages }))
  }

  connection.on('message', message => {
    if (message.type === 'utf8') {
      console.log(new Date() + ' Received Message: ' + message.utf8Data)

      if (IsJsonString(message.utf8Data)) {
        const messageObj = JSON.parse(message.utf8Data)
        if (messageObj.type === 'username') {
          connection.username = messageObj.text
        }
        console.log(`Username ${messageObj.text} set`)
      } else {
        let obj = {
          user: connection.username,
          time: new Date().getTime(),
          text: message.utf8Data
        }

        history_messages.push(obj)
        history_messages = history_messages.slice(-100)

        const json = JSON.stringify({ type: 'message', data: obj })
        clients.forEach(client => {
          client.sendUTF(json)
        })
      }
    }
  })

  connection.on('close', connection => {
    console.log(new Date() + ' Peer ' + connection.remoteAddress + ' disconnected.')
    clients.splice(connection.index, 1)
  })
})

const IsJsonString = str => {
  try {
    JSON.parse(str)
  } catch (e) {
    return false
  }
  return true
}
