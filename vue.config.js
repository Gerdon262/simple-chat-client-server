module.exports = {
  chainWebpack: config => {
    // Manually set the entry point
    config
      .entry('app')
      .clear()
      .add('./src/js/main.js')
    // Manually set template path
    config.plugin('html').tap(args => {
      args[0].template = './index.html'
      return args
    })
  }
}
